import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../utility";

const initialState = {
  results: [],
};

const removeResult = (state, action) => {
  const results = [...state.results];
  results.splice(action.payload, 1);
  return updateObject(state, { results });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ADD_TO_RESULTS: {
      return {
        ...state,
        results: [...state.results, { id: new Date(), value: action.payload }],
      };
    }
    case actionTypes.REMOVE_ITEM_FROM_RESULT:
      return removeResult(state, action);
    default:
      return { ...state };
  }
};

export default reducer;
