import * as actionTypes from "./actionTypes";

export const addToResults = (result) => ({
  type: actionTypes.ADD_TO_RESULTS,
  payload: result,
});

export const asyncAddToResults = (result) => (dispatch, getState) => {
  // Call API
  setTimeout(() => {
    // console.log(getState().ctr.counter);
    dispatch(addToResults(result));
  }, 2000);
};

export const removeItemFromResult = (index) => ({
  type: actionTypes.REMOVE_ITEM_FROM_RESULT,
  payload: index,
});
