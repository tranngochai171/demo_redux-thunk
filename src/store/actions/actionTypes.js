export const INCREMENT = "INCREMENT";
export const DECREMENT = "DECREMENT";
export const ADD = "ADD";
export const SUBTRACT = "SUBTRACT";
export const ADD_TO_RESULTS = "ADD_TO_RESULTS";
export const REMOVE_ITEM_FROM_RESULT = "REMOVE_ITEM_FROM_RESULT";
