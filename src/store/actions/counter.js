import * as actionTypes from "./actionTypes";

export const increment = () => ({ type: actionTypes.INCREMENT });

export const decrement = () => ({ type: actionTypes.DECREMENT });

export const add = () => ({ type: actionTypes.ADD, payload: 10 });

export const subtract = () => ({ type: actionTypes.SUBTRACT, payload: 8 });
