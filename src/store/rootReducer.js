import { combineReducers } from "redux";
import counterReducer from "./reducers/counter";
import resultReducer from "./reducers/result";

export const rootReducer = combineReducers({
  ctr: counterReducer,
  res: resultReducer,
});

export const logger = (store) => (next) => (action) => {
  console.log("[Middleware] Dispatching", action);
  return next(action);
};
